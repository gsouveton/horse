#ifndef _FILE_H_
#define _FILE_H_

#include "global.h"

void F_CannotOpen ( const char * name );

FILE * F_Open ( const char * name, const char * type );

void F_Close ( FILE * f );

void F_CloseAll ( void );

#endif

