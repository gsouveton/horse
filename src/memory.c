#include "memory.h"

void M_BadAlloc ( char * l )
{
	P_PrintLog ( "Fatal error : cannot allocate memory in '%s'\n" , l );
	exit ( -1 );
}

void * M_Alloc ( size_t s , char * l )
{
	void * x = (void*) malloc ( s );
	if ( x == NULL )
		M_BadAlloc ( l );
	return x;
}
