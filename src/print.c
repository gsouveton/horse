#include "print.h"

#ifndef LOGFILE
	#define LOGFILE "server.log"
#endif

void P_PrintLog ( const char * format , ... )
{
	FILE       * logfile;
	va_list      argptr;

	logfile = F_Open ( LOGFILE , "a+" );

	va_start ( argptr , format );
	vfprintf ( logfile , format , argptr );
	va_end   ( argptr );
	
	F_Close ( logfile );
}

void P_PrintDebug ( const char * format , ... )
{
	va_list  argptr;

	va_start ( argptr , format );
	vfprintf ( stderr , format , argptr );
	va_end   ( argptr );
}

#ifndef P_PrintError
	#define P_PrintError P_PrintDebug
#endif

#if 0

	#ifndef P_Error
		#define P_Error P_PrintError
	#endif

#else

	void P_Error ( const char * format , ... )
	{
		FILE       * logfile;
		va_list      argptr;

		logfile = F_Open ( LOGFILE , "a+" );

		va_start ( argptr , format );
		vfprintf ( logfile , format , argptr );
		vfprintf ( stderr , format , argptr );
		va_end   ( argptr );

		F_Close ( logfile );
	}

#endif

void P_Print ( const char * format , ... )
{
	va_list      argptr;

	va_start ( argptr , format );
	vfprintf ( stdout , format , argptr );
	va_end   ( argptr );
	
	fflush(stdout);
}

void P_PrintFile ( const char * filename , const char * format , ... )
{
	FILE       * file;
	va_list      argptr;

	file = F_Open ( filename , "a+" );

	va_start ( argptr , format );
	vfprintf ( file , format , argptr );
	va_end   ( argptr );
	
	F_Close ( file );
}
