#include "types.h"
#include "game.h"

/***************************************************************
                                                               
    Module game.c  -------------------------------------------->
                                                               
***************************************************************/

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                                      ////
////                            P  U  B  L  I  C         F  U  N  C  T  I  O  N  S                        ////
////                                                                                                      ////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


game_t * G_Create ( char * logname )
#if 0

	Creates a new game.

#endif
{
	game_t * game = M_Alloc ( sizeof ( game_t ) , "G_Create" );

	G_InitialiseGrid ( game -> grid );

	R_Memcpy ( game -> logname , logname , max  ( sizeof ( logname ) , 81 ) );
	
	game -> ended = 0;

	game -> winner = PL_NONE;

	game -> activePlayer = PL_WHITE ; // White player starts.
	return game;
}

void G_Destroy ( game_t * game )
{
	if ( game )
		free ( game );
	game = NULL;
}

int G_Play ( int x , int y , player_t p , game_t * game )
#if 0

	Returns who s the next player ( PL_WHITE or PL_BLACK ) <=> ( 1 or 2 ).
	If game is locked or ended, game data is modified and G_Play returns 0 ( PL_NONE ).
	If passed move is not a valid one, returns -1.

#endif
{
	move_t * m = VMOV ( x , y , p );
	
	int a = G_IsValidMove ( m , game );
	int w , b;

	DMOV ( m );

/**/
	if ( a != 1 ) // FIXME : Suggest to add specific actions to deal with different error nums.
		return -1;
/**/
	// Let's export grid to update log.
	G_ExportGrid ( game -> logname , game -> grid );

	// Let's see who's next player.
	game -> activePlayer = G_NextPlayer ( game );

	if ( ! game -> activePlayer )
	{
		game -> ended = 1;
		b = G_CountPoints ( game -> grid , PL_WHITE );
		w = G_CountPoints ( game -> grid , PL_BLACK );
		/*b == w ? ( game -> winner = 3 ) : ( game -> winner = ( ( w < b ) + 1 ) ); // FIXME*/
		if ( b <= w ) game -> winner |= 1;
		if ( b >= w ) game -> winner |= 2;
	}
	return game -> activePlayer;
}

int G_BoardState ( game_t * game )
#if 0

	Returns 0 if game is being played.
	Returns 1 if white player has won.
	Returns 2 if black player has won.
	Returns 3 in case of draw match.

#endif
{
	if ( game -> ended )
		return game -> winner;
	return 0;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                                      ////
////                          P  R  I  V  A  T  E         F  U  N  C  T  I  O  N  S                       ////
////                                                                                                      ////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////





move_t * VMOV ( int x , int y , player_t p )  // FIXME : Are x and y correct ?
{
	move_t * k = M_Alloc ( sizeof ( move_t ) , "VMOV" );
	k -> x = x;
	k -> y = y;
	k -> player = p;
	return k;
}

#ifndef DMOV
	#define DMOV(x) free(x)
#endif

/**
int G_OtherPlayer ( player_t p ) // Don't pass PL_NONE to this function.
{
        // Returns 1 if 2 is passed and 2 if 1 is passed.
        return  2 - (( p + 1 ) % 2 );
}
**/


int G_OtherPlayer ( player_t p ) // FIXED : this function can be used with PL_NONE
#if 0

        This function return 2 if 1 passed ; 1 if 2 passed and 0 if 0 passed.

#endif
{
        return ( 3 - p ) % 3;
}

int G_CountPoints ( grid_t grid , player_t player )
#if 0

        This function counts the number of points of target player on the grid.

#endif
{
        size_t i;
        int c = 0;
        for ( i = 0 ; i < 64 ; ++i )
                c += ( * ( grid + i ) == player ); // ** or just * ?
        return c;
}

void G_InitialiseGrid ( grid_t grid )
{
	grid = R_Memset ( grid , PL_NONE , sizeof ( grid_t ) ); // sizeof ( grid_t ) , not sizeof ( grid ) !
                                                            // note : grid is a pointer (its size's 4bytes, not 256).
	grid[27] = PL_WHITE; grid[28] = PL_BLACK; grid[35] = PL_BLACK; grid[36] = PL_WHITE;
}

void G_ApplyMove ( move_t * move , game_t * game )
#if 0

	0x80   0x40   0x20
	0x01   XXXX   0x10
	0x02   0x04   0x08

#endif
{
	char around = 0;
	/*int i;*/
	int position = move -> x + 8 * move -> y;
	game -> grid [ position ] = move -> player;
	player_t opponent = G_OtherPlayer ( move -> player );

	if ( position - 1 - 8 >= 0 && game -> grid [ position - 9 ] == opponent )
		around |= 0x80;
	if ( position - 0 - 8 >= 0 && game -> grid [ position - 8 ] == opponent )
		around |= 0x40;
	if ( position + 1 - 8 >= 0 && game -> grid [ position - 7 ] == opponent )
		around |= 0x20;
	if ( position - 1 + 0 >= 0 && game -> grid [ position - 1 ] == opponent )
		around |= 0x01;
	if ( position + 1 + 0 >= 0 && game -> grid [ position + 1 ] == opponent )
		around |= 0x10;
	if ( position + 8 - 1 >= 0 && game -> grid [ position + 7 ] == opponent )
		around |= 0x02;
	if ( position + 8 + 0 >= 0 && game -> grid [ position + 8 ] == opponent )
		around |= 0x04;
	if ( position + 8 + 1 >= 0 && game -> grid [ position + 9 ] == opponent )
		around |= 0x08;
 
	//printgrid ( game -> grid );
	
	#if 0

		0x80   0x40   0x20
		0x01   XXXX   0x10
		0x02   0x04   0x08

	#endif

	int dt , oi , oj , pi , pj;
	for ( dt = 1  ; dt <= 0x80 ; dt *= 2 )
	{
		//fprintf ( stderr , "pos : %d , %d ar : %#x\n" , move -> x , move -> y , around );
		pi = 0, pj = 0;
		if ( around && ((around & dt) == dt) )
		{
			if ( dt == 0x80 ){ pi = -1; pj =-1; }
			if ( dt == 0x40 ){ pi = 0; pj =-1; }
			if ( dt == 0x20 ){ pi = 1; pj =-1; }
			if ( dt == 0x10 ){ pi = 1; pj =0; } ////////////////////////////////// FUCK ! 
			if ( dt == 0x08 ){ pi = 1; pj =1; }
			if ( dt == 0x04 ){ pi = 0; pj =1; }
			if ( dt == 0x02 ){ pi = -1; pj =1; }
			if ( dt == 0x01 ){ pi = -1; pj =0; }

			oj = move->y + pj ; oi = move->x + pi;
			while ( oj <= 7 && oj >= 0 && oi <= 7 && oi >= 0 )
			{
				if ( game -> grid [ oi + 8 * oj ] != opponent )
				{
					if ( game -> grid [ oi + 8 * oj ] == move -> player )
					{
						oi -= pi;
						oj -= pj;
						while ( game -> grid [ oi + 8 * oj ] != move -> player )
						{
							game -> grid [ oi + 8 * oj ] = move -> player;
							oi -= pi;
							oj -= pj;
						}				
					}
					break;
				}
				oj += pj; oi += pi;
			}
			around &= ( ~ dt );
		}
	
	}
	
	
}

int G_Clamped ( int x )
{
	return ( x >= 0 && x <= 7 );
}

int G_IsValidMove( move_t * move, game_t * game )
#if 0

        Returns -1 if passed move is incoherent or malformed.
        Returns  0 if passed move is coherent but not allowed in current game context.
        Returns  1 if passed move is ok.

        This function uses the recrusive axiom : "game -> grid is already a valid grid".
        If this function is called each move, as the original grid is valid, all grid
        passed to this function are supposed to be valid ones.

#endif
{
        int points_old , points_new;

        if ( ! game )
        	return -1; // Board doesn't exist.
        
        if ( ! ( move && game -> grid ) )
               return -1; // Malformed data.
				
		if ( ! G_Clamped ( move -> x ) || ! G_Clamped ( move -> y ) )
			return -1; // Dumbass' client \o/

        if ( game -> grid [ move -> x + move -> y * 8 ] )
                return 0; // Case isn't empty. 

        points_old = G_CountPoints ( game -> grid , move -> player );

        // Let's play the move. Anyway, if it's not legal the client will be forced to get the fuck out.
        G_ApplyMove ( move , game );
      
	points_new = G_CountPoints ( game -> grid , move -> player );

        // A legal move must return at least 1 piece.
        if ( points_new - points_old <= 1 ) // There's at least one new piece, let's count them from 2.
        {
      		// On defait le coup qui etait forcement fait sur une case vide !!!!!!!!!!!!!
        	game -> grid [ move -> x + 8 * move -> y ] = 0; // Saloperie de grille.
                return 2;
	}
        return 1;
}

int G_ExportGrid ( char * logname , grid_t grid )
{
	size_t i;
	gamelog_t log = NULL;
	
	log = F_Open ( logname , "a+" );

	if ( ! log )
		return -1;

	for ( i = 0 ; i < sizeof ( grid_t ) / sizeof ( player_t ) ; i++ )
	{
		switch ( grid [ i ] )
		{
			case PL_NONE : fputc ( '0' , log ); break;
			case PL_WHITE : fputc ( '1' , log ); break;
			case PL_BLACK : fputc ( '2' , log ); break;
		}
		if ( i != sizeof ( grid_t ) / sizeof ( player_t ) - 1)
			fputc ( ',' , log );
	}
	fputc ( '\n' ,log);
	F_Close ( log );
	return 0;
}

int G_NextPlayer ( game_t * g )
#if 0

	Returns PL_WHITE , PL_BLACK or PL_NONE.

#endif
{
	int i;
	move_t * k;
	
	game_t * tmp = malloc ( sizeof ( game_t ));

	R_Memcpy ( tmp , g , sizeof ( game_t ) );

	// Let's suppose next player is the one who didn't play last move.
	tmp -> activePlayer = G_OtherPlayer ( tmp -> activePlayer );

	for ( i = 0 ; i < 64 ; i++ )
	{
		k = VMOV ( i % 8 , i / 8 , tmp -> activePlayer );
		if ( G_IsValidMove ( k , tmp ) == 1 )
			return tmp -> activePlayer;		
		DMOV ( k );
	}
	
	// If not, let's try with the other player.
	R_Memcpy ( tmp , g , sizeof ( game_t ) );

	for ( i = 0 ; i < 64 ; i++ )
	{
		VMOV ( i % 8 , i / 8 , tmp -> activePlayer );
		if ( G_IsValidMove ( k , tmp ) == 1 )
			return tmp -> activePlayer;		
		DMOV ( k );
	}

	R_Memcpy ( tmp , g , sizeof ( game_t ) );

	// Wimp out ( end of game ).
	return PL_NONE;
}
