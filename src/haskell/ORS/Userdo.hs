module ORS.Userdo(userDo) where

import ORS.Types
import ORS.ConcurrentTools
import ORS.NetCommandTools
import ORS.Board
import ORS.Env
import qualified ORS.SQL as SQL
import ORS.Game
import ORS.Board
import ORS.Tournament

import Data.Time
import Data.String.Utils
import System.IO
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Concurrent.STM.TVar
import Control.Monad.STM
import Control.Monad
import Prelude hiding(catch)
import Control.Exception
import System.Random(randomIO)

userDo env uid s t = catch
		(do
		putStrLn $ (show uid) ++ " : " ++ s
		cmd <- return $! (toNetCommand s)
		case cmd of
			DISCONNECT -> disconnect env uid
			CONNECT (LOGWITH username password) -> connect env uid username password
			P_LIST Pl_REQ -> plist env uid
			R_GAME rg -> rgame env uid rg
			IN_GAME gid ig -> ingame env uid gid ig t
			T_LIST Tl_REQ -> tlist env uid
			R_TOURNAMENT ( JOIN tId )-> rtournamentj env uid tId
			R_TOURNAMENT Rt_CANCEL -> rtournamentc env uid
			_ -> return ()
		)
		(ncHandler env uid)
	where 	ncHandler :: TVar Env -> UserId -> NetCommandException -> IO ()
		ncHandler env uid _ = do
			putStrLn $ "User : " ++ (show uid) ++ " unable to translate"
			disconnect env uid

--TODO: Lors de la déco d'un user, le virer proprement, ie arreter les parties et tousssssa
disconnect :: TVar Env -> UserId -> IO ()
disconnect env uId = do
	user <- atomically $ getUserByUid env uId
	talkTo user DISCONNECT
	(killThread $ inputThread user)
	game <- atomically $ getGameByUserId env uId
	case game of
		Nothing -> return ()
		Just game -> if ( not $ playing game )
				then do rgame env uId $ Rg_cERROR (gid game)
				else do in_game_end env (gid game) uId TIMEOUT
	atomically $ delUser env uId -- MUST BE THE _*_LAST_*_ LINE 

connect :: TVar Env -> UserId -> String -> String -> IO ()
connect env uid username password = do
	user <- atomically $ getUserByUid env uid
	unless ( (uuid user) /= 0 ) $ do 
		uuid_ <- SQL.login username password
		if ( uuid_ /= 0 )
			then do
				atomically $ updateUser env uid (user{uuid = uuid_, name = username})
				talkTo user $ CONNECT C_ACK
			else talkTo user $ CONNECT C_ERROR

plist :: TVar Env -> UserId -> IO ()
plist env uId = do
	user <- atomically $ getUserByUid env uId
	unless ( (uuid user) == 0 ) $ do
		users <- atomically $ getAvailableUsers env uId
		talkTo user $ P_LIST Pl_BEGIN
		mapM_ (\u -> (talkTo user $ P_LIST $ Pl_ENTRY (uid u) (name u))) users
		talkTo user $ P_LIST Pl_END

tlist :: TVar Env -> UserId -> IO ()
tlist env uId = do
	user <- atomically $ getUserByUid env uId
	unless ( (uuid user) == 0 ) $ do
		tournaments <- atomically $ getAvailableTournaments env
		talkTo user $ T_LIST Tl_BEGIN
		mapM_ (\t -> (talkTo user $ T_LIST $ Tl_ENTRY
			(tid t) (seats t) ((seats t) - (length (players t) )) (limit t))) tournaments
		talkTo user $ T_LIST Tl_END
		
rtournamentj :: TVar Env -> UserId -> TournamentId -> IO ()
rtournamentj env uId tId = do
	u1 <- atomically $ getUserByUid env uId
	when ( (uuid u1 /= 0) && ((state u1) == [])) $ do
		t <- atomically $ getTournamentFromTid env tId
		case t of
			Nothing -> talkTo u1 (R_TOURNAMENT Rt_ERROR)
			Just t' -> do
				if ( isReadyTournament t' )
					then do
						talkTo u1 (R_TOURNAMENT Rt_ERROR)
					else do
						atomically $ updateTournament env tId (addUserToTournament t' uId)
						atomically $ updateUser env uId (u1{state=[InTournament]})
						talkTo u1 (R_TOURNAMENT Rt_ACK)
						when (isReadyTournament (addUserToTournament t' uId)) $ do launchtournament env tId
	return ()

rtournamentc :: TVar Env -> UserId -> IO ()
rtournamentc env uId = do
	u1 <- atomically $ getUserByUid env uId
	when ( (uuid u1 /= 0) && (state u1) /= []) $ do
		t <- atomically $ getTournamentFromUid env uId
		case t of
			Nothing -> return ()
			Just t' -> if ( isReadyTournament t' ) then talkTo u1 (R_TOURNAMENT Rt_ERROR)
					else do
						atomically $ updateTournament env (tid t') (delUserFromTournament t' uId)
						atomically $ updateUser env uId (u1{state=[]})
						talkTo u1 (R_TOURNAMENT Rt_ACK)
	return ()

rgame :: TVar Env -> UserId -> R_game -> IO ()
rgame env uId rg = do
	u1 <- atomically $ getUserByUid env uId
	when ( uuid u1 /= 0 ) $ do
		case rg of
			RANDOM t -> do
				avail <- atomically $ getAvailableUsers env uId
				if ( (length avail) /= 0 )
					then do
						r <- randomIO
						let indice = (r `mod` (length avail))
						rgamew env uId (uid (avail!!indice)) t
					else do
						talkTo u1 $ R_GAME $ Rg_sERROR
			WITH id t -> do
				cond <- atomically $ userExists env id
				if (cond)
					then do
						rgamew env uId id t
					else do
						talkTo u1 $ R_GAME $ Rg_sERROR
			Rg_cACK id -> do
				cond' <- atomically $ isTakingPartIn env uId id
				when (cond') $ do
					cond <- atomically $ gameExists env id
					if ( cond )
					then do
						game <- atomically $ getGameByGid env id
						unless ( playing game ) $ do
							if ( uId == p1 game )
								then atomically $ updateGame env id game{p1_accepted=True}
								else atomically $ updateGame env id game{p2_accepted=True}
							game <- atomically $ getGameByGid env id
							when ( and [(p1_accepted game),(p2_accepted game)] ) $ launchgame env id
					else do
						talkTo u1 $ R_GAME $ Rg_sERROR
			Rg_cERROR id -> do
				cond' <- atomically $ isTakingPartIn env uId id
				when (cond') $ do
					cond <- atomically $ gameExists env id
					if ( cond )
					then do
						game <- atomically $ getGameByGid env id
						u1 <- atomically $ getUserByUid env $ p1 game
						u2 <- atomically $ getUserByUid env $ p2 game
						atomically $ updateUser env (uid u1) u1{state = (filter (\s -> s /= RequestingGame) (state u1))}
						atomically $ updateUser env (uid u2) u2{state = (filter (\s -> s /= RequestingGame) (state u2))}
						unless ( playing game ) $ do
							if ( uId == p1 game )
							then do
								user <- atomically $ getUserByUid env (p2 game)
								talkTo user $ R_GAME Rg_sERROR
							else do
								user <- atomically $ getUserByUid env (p1 game)
								talkTo user $ R_GAME Rg_sERROR
						delBoard $ board game
						atomically $ delGame env id
					else do 
						atomically $ updateUser env uId u1{state = (filter (RequestingGame /= )(state u1))}
			Rg_cREJECT id -> rgame env uId (Rg_cERROR id)

rgamew env uid1 uid2 t = do
	u1 <- atomically $ getUserByUid env uid1
	u2 <- atomically $ getUserByUid env uid2
	if ( (state u1 == []) && (state u2 == []) )
		then do
		time <- getCurrentTime
		let str = "logs/game_" ++ (show (uuid u1)) ++ "_" ++ (show (uuid u2)) ++ "_" ++ (replace " " "_" (show time)) ++ ".hlog"
		game <- newGame u1 u2 str t
		gid <- atomically $ addGame env game
		talkTo u1 $ R_GAME $ Rg_sACK
		talkTo u1 $ R_GAME $ FINALIZE gid t
		talkTo u2 $ R_GAME $ FINALIZE gid t
		atomically $ do
			updateUser env (uid u1) u1{state = [RequestingGame]}
			updateUser env (uid u2) u2{state = [RequestingGame]}
		else do
		talkTo u1 $ R_GAME $ Rg_sERROR

launchgame :: TVar Env -> GameId -> IO ()
launchgame env gId = do
	game <- atomically $ getGameByGid env gId
	u1 <- atomically $ getUserByUid env $ p1 game
	u2 <- atomically $ getUserByUid env $ p2 game
	startGame game (uuid u1) (uuid u2)
	atomically $ do
		updateGame env gId game{playing=True}
		updateUser env (uid u1) u1{state = (InGame:(filter (\s -> s /= RequestingGame) (state u1)))}
		updateUser env (uid u2) u2{state = (InGame:(filter (\s -> s /= RequestingGame) (state u2)))}
	in_game_control env gId

ingame :: TVar Env -> UserId -> GameId -> In_game -> UTCTime -> IO ()
ingame env uId gId ig t = do
	cond' <- atomically $ isTakingPartIn env uId gId
	if (cond')
		then do
		let PLAY (x,y) = ig
		game <- atomically $ getGameByGid env gId
		u1 <- atomically $ getUserByUid env (p1 game)
		u2 <- atomically $ getUserByUid env (p2 game)
		let timeout = (diffUTCTime t (last_played game)) > ((timelimit game) + 1)
		if ( not timeout)
			then do
			res <- playMove (board game) x y (getParsableNextUser game)
			case res of
				-1 -> putStrLn "mauvais coup joué" >> in_game_end env gId uId ERROR
				0 -> do
					in_game_ack_and_inform env gId (x,y)
					game_state <- finalState (board game)
					case game_state of
						0 -> putStrLn "partie annoncée finie mais non finie !!! kitty !!!"
						2 -> in_game_end env gId (p1 game) WIN
						1 -> in_game_end env gId (p2 game) WIN
						3 -> in_game_end env gId uId DRAW
				1 -> do
					in_game_ack_and_inform env gId (x,y)
					atomically $ updateGame env gId (game{next=(p1 game)})
					in_game_control env gId
				2 -> do
					in_game_ack_and_inform env gId (x,y)
					atomically $ updateGame env gId (game{next=(p2 game)})
					in_game_control env gId
			else do
			in_game_end env gId uId TIMEOUT
		else do
		user <- atomically $ getUserByUid env uId
		talkTo user $ IN_GAME gId Ig_ERROR

in_game_control :: TVar Env -> GameId -> IO ()
in_game_control env gId = do
	game <- atomically $ getGameByGid env gId
	u1 <- atomically $ getUserByUid env $ p1 game
	u2 <- atomically $ getUserByUid env $ p2 game
	if ( next game == p1 game )
		then do
			talkTo u1 $ IN_GAME (gid game) YOURTURN
			talkTo u2 $ IN_GAME (gid game) WAIT
		else do
			talkTo u2 $ IN_GAME (gid game) YOURTURN
			talkTo u1 $ IN_GAME (gid game) WAIT
	time <- getCurrentTime
	atomically $ do
		updateGame env gId game{last_played=time}

in_game_ack_and_inform :: TVar Env -> GameId -> Coord -> IO ()
in_game_ack_and_inform env gId (x,y) = do
	game <- atomically $ getGameByGid env gId
	u_played <- atomically $ getUserByUid env (next game)
	u_notplayed <- atomically $ getUserByUid env (getOtherUserId game (uid u_played))
	talkTo u_played $ IN_GAME gId Ig_ACK
	talkTo u_notplayed $ IN_GAME gId (PLAYED (x,y))

in_game_end :: TVar Env -> GameId -> UserId -> Reason -> IO ()
in_game_end env gId uId r = do
	game <- atomically $ getGameByGid env gId
	user <- atomically $ getUserByUid env uId
	other_user <- atomically $ getUserByUid env (getOtherUserId game uId)
	t <- atomically $ getTournamentFromGame env gId
	case r of
		WIN -> do
			endGame game (uuid user) 1
			talkTo user $ IN_GAME gId $ Ig_END $ WIN
			talkTo other_user $ IN_GAME gId $ Ig_END $ LOSE
		DRAW -> do
			endGame game (uuid user) 2
			talkTo user $ IN_GAME gId $ Ig_END $ DRAW
			talkTo other_user $ IN_GAME gId $ Ig_END $ DRAW
		ERROR -> do
			endGame game (uuid other_user) 1
			talkTo user $ IN_GAME gId $ Ig_ERROR
			talkTo user $ IN_GAME gId $ Ig_END $ ERROR
			talkTo other_user $ IN_GAME gId $ Ig_END $ WIN
		TIMEOUT -> do
			endGame game (uuid other_user) 1
			talkTo user $ IN_GAME gId $ Ig_ERROR
			talkTo user $ IN_GAME gId $ Ig_END $ TIMEOUT
			talkTo other_user $ IN_GAME gId $ Ig_END $ WIN
	atomically $ do
		updateUser env (uid user) $ user{state = ((filter (\s -> s /= InGame) (state user)))}
		updateUser env (uid other_user) $ other_user{state = ((filter (\s -> s /= InGame) (state other_user)))}
	delBoard (board game)
	atomically $ delGame env gId
	case r of
		WIN -> maybe (return ()) (\t -> endGameInTournament env (tid t) (uid user) (uid other_user) 1 ) t
		DRAW -> maybe (return ()) (\t -> endGameInTournament env (tid t) (uid user) (uid other_user) 0 ) t
		ERROR -> maybe (return ()) (\t -> endGameInTournament env (tid t) (uid user) (uid other_user) (-1) ) t
		TIMEOUT -> maybe (return ()) (\t -> endGameInTournament env (tid t) (uid user) (uid other_user) (-1) ) t

launchtournament :: TVar Env -> TournamentId -> IO ()
launchtournament env tId = do
	t <- atomically $ getTournamentFromTid env tId
	case t of
		Nothing -> undefined
		Just t' -> do
			atomically $ updateTournament env tId (calculateToBePlayedTournament t')
			runNextGameTournament env tId
			
endGameInTournament :: TVar Env -> TournamentId -> UserId -> UserId -> Int -> IO ()
endGameInTournament env tId uid1 uid2 res = do
	putStrLn "Tournament game ended"
	t' <- atomically $ getTournamentFromTid env tId
	case t' of
		Nothing -> error "unhandled tournament sequence"
		Just t -> do
			atomically $ updateTournament env tId (writeResultTournament t uid1 uid2 res)
			runNextGameTournament env tId

runNextGameTournament :: TVar Env -> TournamentId -> IO ()
runNextGameTournament env tId = do
	t' <- atomically $ getTournamentFromTid env tId
	case t' of
		Nothing -> error "unhandled tournament sequence"
		Just t -> do
			case (toBePlayed t) of
				[] -> finalizeTournament env tId
				(uid1,uid2):xs -> do
					u1 <- atomically $ getUserByUid env uid1
					u2 <- atomically $ getUserByUid env uid2
					time <- getCurrentTime
					let str = "logs/game_" ++ (show (uuid u1)) ++ "_" ++ (show (uuid u2)) ++ "_" ++ (replace " " "_" (show time)) ++ ".hlog"
					game <- newGame u1 u2 str (limit t)
					gid <- atomically $ addGame env game
					talkTo u1 $ R_GAME $ FINALIZE gid (limit t)
					talkTo u2 $ R_GAME $ FINALIZE gid (limit t)
					atomically $ do
						updateUser env (uid u1) u1{state = RequestingGame:(state u1)}
						updateUser env (uid u2) u2{state = RequestingGame:(state u2)}
						updateTournament env tId (t{current_games = [gid],toBePlayed=xs})

finalizeTournament :: TVar Env -> TournamentId -> IO ()
finalizeTournament env tId = do
	t <- atomically $ getTournamentFromTid env tId
	case t of
		Nothing -> return ()
		Just t' -> do
			userlist <- atomically $ mapM (\uId -> getUserByUid env uId) (players t')
			mapM_ (\u -> atomically $ updateUser env (uid u) (u{state=[]})) userlist
			mapM_ (\u -> talkTo u $ R_TOURNAMENT (Rt_END 0)) (userlist)
