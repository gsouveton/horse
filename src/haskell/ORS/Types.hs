{-|
	Module	: ORS.Types
	Version	: 0.2
	State	: work in progress
	Author	: Jonathan GAYVALLET <jonathan.mael.gayvallet@gmail.com>
	Licence	: BSD
-}

module ORS.Types where


-- Imports
import System.IO
import Control.Concurrent
import Control.Concurrent.STM.TChan
import ORS.Board
import Data.Time.Clock

-- Functions types

type ClientHandler = Handle -> TChan (String,UTCTime) -> ThreadId -> IO ()
type Finalizer = TChan (String,UTCTime) -> IO ()

-- Basic types synonyms
type UserName = String
type Password = String
type UserId = Int
type GameId = Int
type TournamentId = Int
type Time = NominalDiffTime
type Coord = (Int, Int)


-- Network Commands from the RC1
data NetCommand = CONNECT Connect | DISCONNECT | P_LIST P_list | R_GAME R_game
		| IN_GAME GameId In_game | T_LIST T_list | R_TOURNAMENT R_tournament
	deriving (Show)

data Connect = LOGWITH UserName Password | C_ACK | C_ERROR
	deriving (Show)

data P_list = Pl_REQ | Pl_BEGIN | Pl_END | Pl_ENTRY UserId UserName
	deriving (Show)

data R_game = Rg_sACK | Rg_sERROR | Rg_sREJECT | Rg_cACK GameId | Rg_cERROR GameId
		| Rg_cREJECT GameId | RANDOM Time | WITH UserId Time | FINALIZE GameId Time
	deriving (Show)

data In_game = Ig_ACK | Ig_ERROR | YOURTURN | WAIT | PLAY Coord
		| PLAYED Coord | Ig_END Reason
	deriving (Show)

data Reason = WIN | LOSE | DRAW | ERROR | TIMEOUT
	deriving (Show)

data T_list = Tl_REQ | Tl_BEGIN | Tl_END | Tl_ENTRY TournamentId Int Int Time
	deriving (Show)

data R_tournament = JOIN TournamentId | Rt_ACK | Rt_ERROR | Rt_CANCEL | Rt_END Int
	deriving (Show)
	
	
data User = User
	{	uid		:: UserId,
		uuid		:: UserId,
		name		:: UserName,
		state		:: [UserState],
		inputChannel	:: TChan (String,UTCTime),
		talkTo		:: NetCommand -> IO (),
		inputThread	:: ThreadId
	}	
instance Show User where
	show u = "{User("++ (show (uuid u) ) ++") : " ++ (show (uid u)) ++ " : " ++ (show (name u)) ++ " in state " ++ (show (state u)) ++ "}"
	
	
data UserState = RequestingGame | InGame | InTournament
	deriving (Eq,Show)
	
data Tournament = Tournament -- We may have to alter this type to include the state of the tournament
	{
		tid 		:: TournamentId,
		limit		:: Time,
		seats		:: Int,
		players		:: [UserId],
		toBePlayed	:: [(UserId,UserId)],
		played		:: [((UserId,UserId),Int)],
		current_games	:: [GameId]
	}
	deriving (Show)

data Game = Game
	{
		gid		:: GameId,
		last_played	:: UTCTime,
		p1		:: UserId,
		p2		:: UserId,
		next		:: UserId,
		timelimit	:: Time,
		board		:: Board,
		playing		:: Bool,
		p1_accepted	:: Bool,
		p2_accepted	:: Bool,
		log_name	:: String
	}
instance Show Game where
	show g = "{Game : " ++ (show (gid g)) ++ " between " ++ (show (p1 g)) ++ " and " ++ (show (p2 g)) ++ " Started : " ++ (show (playing g)) ++"}"
	
data Env = Env
	{
		users		:: [User],
		games		:: [Game],
		tournaments	:: [Tournament],
		freeUids	:: [UserId],
		freeGids	:: [GameId],
		freeTids	:: [TournamentId]
	}
