{-|
	Module	: ORS.ConcurrentTools
	Version	: 0.1
	State	: working
	Author	: Jonathan GAYVALLET <jonathan.mael.gayvallet@gmail.com>
	Licence	: BSD
-}

module ORS.ConcurrentTools where

import Prelude hiding (catch)
import System.IO.Error hiding (catch)
import Network ()
import System.IO ()
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM


-- | A wrapper for 'forkIO' that throws exceptions from the new thread to the calling thread
spawn :: IO () -> IO ThreadId
spawn f = do
	mainTID <- myThreadId
	forkIO $ catch f (\err -> throwTo mainTID (err::SomeException))

-- | Read input from 2 'TChan' at the same time, returns as soon as there is something to read
select' :: TChan a -> TChan b -> STM (Either a b)
select' ch1 ch2 = do
	a <- readTChan ch1; return (Left a)
	`orElse` do
	b <- readTChan ch2; return (Right b)
	
select	:: (STM a) -> (STM b) -> STM (Either a b)
select stm1 stm2 = do
	a <- stm1; return (Left a)
	`orElse` do
	b <- stm2; return (Right b)
	
-- | 'tselect' is a function which multiplexes any number of 'TChan's. It will return the data from whichever 'TChan' it can read, along with the "key" value that can be supplied in the pair. This takes advantage of the 'STM' combinators 'orElse' and 'retry' by applying them to a list of actions constructed around the 'TChan's. 
tselect :: [(TChan a, t)] -> STM (a, t)
tselect = foldr orElse retry . map (\(ch, ty) -> (flip (,) ty) `fmap` readTChan ch)

lock :: IO ()
lock = atomically $ retry

handleEOF e
	| isEOFError e = return () -- the socket got closed, this can be expected
	| isIllegalOperation e = return () -- can happen from time to time, nothing to get alarmed about
	| otherwise = putStrLn $ "caught : " ++ (show e) -- unexpected error
