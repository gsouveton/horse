module ORS.Manager (manager) where

import ORS.Types
import ORS.ConcurrentTools
import ORS.NetCommandTools
import ORS.Board
import ORS.Env
import ORS.SQL
import ORS.Userdo

import System.IO
import Control.Concurrent.STM.TChan
import Control.Concurrent.STM.TVar
import Control.Concurrent
import Control.Monad.STM
import Control.Monad
import Prelude hiding(catch)
import Control.Exception
import Data.Time.Clock


manager :: TChan (Handle, TChan (String,UTCTime), ThreadId) -> TVar Env -> IO ()
manager registerTChan env = do
	putStrLn "Manager running"
	forever $ do
		env_ <- atomically $ readTVar env
		putStrLn "--USERS--"
		mapM_ (putStrLn . show) (users env_)
		putStrLn "--/USERS--"
		putStrLn "--GAMES--"
		mapM_ (putStrLn . show) (games env_)
		putStrLn "--/GAMES--"
		putStrLn "--TOURNAMENTS--"
		mapM_ (putStrLn . show) (tournaments env_)
		putStrLn "--/TOURNAMENTS--"
		let userlist = buildSelectiveUserList (users env_)
		action <- atomically $ select
			(readTChan registerTChan)
			(tselect userlist)
		case action of
			Left (h,chan,thread) -> atomically $ addUser env h chan thread
			Right ((s,t),uid) -> userDo env uid s t
