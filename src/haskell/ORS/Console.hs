module ORS.Console where

import ORS.Types
import ORS.ConcurrentTools
import ORS.NetCommandTools
import ORS.Board
import ORS.Env
import ORS.SQL
import ORS.Userdo
import ORS.Manager

import System.IO
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Concurrent.STM.TVar
import Control.Monad.STM
import Control.Monad
import Prelude hiding(catch)
import Control.Exception
import System.Console.Readline

console env = forever $ do
	line <- readline "ORS $ "
	case line of
		Nothing -> end
		Just l -> case l of
			"exit" -> end
			_ -> putStrLn "kitty !"
	where end = myThreadId >>= killThread
