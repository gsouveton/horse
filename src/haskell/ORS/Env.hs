module ORS.Env where

import ORS.Types
import ORS.ConcurrentTools
import ORS.NetCommandTools
import ORS.Tournament

import Control.Concurrent.STM.TChan
import Control.Concurrent.STM.TVar
import Control.Concurrent
import Control.Monad.STM
import System.IO
import Data.Maybe ()
import Data.List
import Data.Time.Clock

-- | Creates a new empty 'Env'
initEnv :: Env
initEnv = Env [] [] [(Tournament 1 10 4 [] [] [] [])] [1..] [1..] [2..]

-- | returns the lowest 'UserId' available and remove it from the pool
getNextUid :: TVar Env -> STM UserId
getNextUid env = do
	e <- readTVar env
	writeTVar env $ e{freeUids=(tail (freeUids e))}
	return (head (freeUids e))
	
-- | puts an 'UserId' back in the pool
releaseUid :: TVar Env -> UserId -> STM ()
releaseUid env uId = do
	e <- readTVar env
	writeTVar env $ e{freeUids=(uId : (freeUids e))}

-- | adds an 'User' from its 'Handle', its incomming 'TChan' and its 'ThreadId'
addUser :: TVar Env -> Handle -> TChan (String,UTCTime) -> ThreadId -> STM ()
addUser env h chan thread = do
	uId <- getNextUid env
	e <- readTVar env
	writeTVar env $ e {users = ( (User uId 0 "" [] chan (f h) thread) : (users e))}
	where f h nc = catch
		(hPutStrLn h (fromNetCommand nc) >> putStrLn (fromNetCommand nc))
		(handleEOF)

-- | modifies the 'User' identified by the given 'UserId'
updateUser :: TVar Env -> UserId -> User -> STM ()
updateUser env uId u = do
	e <- readTVar env
	writeTVar env $ e {users = ( filter (\x -> uId /= uid x) (users e) ) ++ [u] }

-- | removes an 'User'
delUser :: TVar Env -> UserId -> STM ()
delUser env uId = do
	releaseUid env uId
	e <- readTVar env
	writeTVar env $ e  {users = ( filter (\u -> uId /= uid u) (users e)) }

-- | says wether the 'User' identified by the given 'UserId' exists
userExists :: TVar Env -> UserId -> STM Bool
userExists env uId = do
	e <- readTVar env
	return $ maybe (False) (\_->True) (find (\u -> uId == uid u )(users e))

-- | returns the 'User' identified by the given 'UserId', undefined if he does'nt exist
getUserByUid :: TVar Env -> UserId -> STM User
getUserByUid env uId = do
	e <-readTVar env
	maybe (error "unsafe call to getUserById") (return) (find (\u -> uId == uid u )(users e))

-- | get the full 'User' list
getUsers :: TVar Env -> STM [User]
getUsers env = do
	e <- readTVar env
	return (users e)

-- | get every available 'User' not being identified by the given 'UserId'
getAvailableUsers :: TVar Env -> UserId -> STM [User]
getAvailableUsers env uId = do
	e <- readTVar env
	return (filter (\u -> uuid u /= 0) (filter (\u -> uId /= uid u) ((filter (\u -> [] == state u ) (users e)))))

-- | builds the list used by 'tselect'
buildSelectiveUserList :: [User] -> [(TChan (String,UTCTime), UserId)]
buildSelectiveUserList = map (\u -> (inputChannel u, uid u))

-- | returns the lowest available 'GameId' and remove it from the pool
getNextGid :: TVar Env -> STM GameId
getNextGid env = do
	e <- readTVar env
	writeTVar env $ e{freeGids=(tail (freeGids e))}
	return (head (freeGids e))

-- | puts the given 'GameId' back in the pool
releaseGid :: TVar Env -> GameId -> STM ()
releaseGid env gId = do
	e <- readTVar env
	writeTVar env $ e{freeGids=(gId : (freeGids e))}

-- | add a 'Game'
addGame :: TVar Env -> Game -> STM GameId
addGame env game = do
	gId <- getNextGid env
	e <- readTVar env
	writeTVar env $ e {games = ( (game{gid=gId}) : (games e))}
	return gId

-- | says wether a 'Game' identified by the given 'GameId' exists
gameExists :: TVar Env -> GameId -> STM Bool
gameExists env gId = do
	e <- readTVar env
	return $ maybe (False) (\_->True) (find (\g -> gId == gid g )(games e))

-- | returns the 'Game' identified by the given 'GameId', undefined if it doesn't exist
getGameByGid :: TVar Env -> GameId -> STM Game
getGameByGid env gId = do
	e <- readTVar env
	maybe (error "unsafe call to getGameByGid") (return) (find (\g -> gId == gid g )(games e))

-- | modifies the 'Gale' identified by the given 'GameId'
updateGame :: TVar Env -> GameId -> Game -> STM ()
updateGame env gId g = do
	e <- readTVar env
	writeTVar env $ e {games = ( g : (( filter (\x -> gId /= gid x) (games e))))}

-- | may return the 'Game' a given 'User' is taking part in
getGameByUserId :: TVar Env -> UserId -> STM (Maybe Game)
getGameByUserId env uId = do
	e <- readTVar env
	return $ find (\g -> p1 g == uId || p2 g == uId) (games e)

-- | says wether the given 'User' is taking par in the given 'Game'
isTakingPartIn :: TVar Env -> UserId -> GameId -> STM Bool
isTakingPartIn env uId gId = do
	e <- readTVar env
	return $ maybe (False) (const True) (find (\g -> gid g == gId && ( p1 g == uId || p2 g == uId )) (games e))

-- | removes a 'Game'
delGame :: TVar Env -> GameId -> STM ()
delGame env gId = do
	releaseGid env gId
	e <- readTVar env
	writeTVar env $ e {games = ( filter (\g -> gId /= gid g) (games e)) }

-- | returns the lowest available 'TournamentId' and removes it from the pool
getNextTid :: TVar Env -> STM TournamentId
getNextTid env = do
	e <- readTVar env
	writeTVar env $ e{freeTids=(tail (freeTids e))}
	return (head (freeTids e))
	
-- | put a 'TournamentId' back in the pool
releaseTid :: TVar Env -> TournamentId -> STM ()
releaseTid env tId = do
	e <- readTVar env
	writeTVar env $ e{freeTids=(tId : (freeTids e))}

-- | adds a 'Tournament'
addTournament :: TVar Env -> Tournament -> STM ()
addTournament env t = do
	tId <- getNextTid env
	env_ <- readTVar env
	writeTVar env $ env_{tournaments=((t{tid=tId}):(tournaments env_))}

-- | removes a 'Tournament'
delTournament :: TVar Env -> TournamentId -> STM ()
delTournament env tId = do
	releaseTid env tId
	env_ <- readTVar env
	writeTVar env $ env_{tournaments=(filter (\t -> tId /= tid t) $ tournaments env_)}

-- | modifies the 'Tournament' identified by the given 'TournamentId'
updateTournament :: TVar Env -> TournamentId -> Tournament -> STM ()
updateTournament env tId t = do
	e <- readTVar env
	writeTVar env $ e {tournaments = ( t : (( filter (\x -> tId /= tid x) (tournaments e))))}

-- | may return the 'Tournament' a game is part of
getTournamentFromGame :: TVar Env -> GameId -> STM ( Maybe Tournament )
getTournamentFromGame env gId = do
	e <- readTVar env
	return $ find (\t -> or ( (map) (\g -> gId == g) (current_games t) ) ) (tournaments e)
	
getAvailableTournaments :: TVar Env -> STM [Tournament]
getAvailableTournaments env = do
	e <- readTVar env
	return [ x | x <- tournaments e , not (isReadyTournament x)]
	
getTournamentFromTid :: TVar Env -> TournamentId -> STM ( Maybe Tournament )
getTournamentFromTid env tId = do
	e <- readTVar env
	return $ find (\t -> tId == tid t) (tournaments e)
	
getTournamentFromUid :: TVar Env -> UserId -> STM ( Maybe Tournament )
getTournamentFromUid env uId = do
	e <- readTVar env
	return $ find (\t -> or ( (map) (\u -> uId == u) (players t) ) ) (tournaments e)
