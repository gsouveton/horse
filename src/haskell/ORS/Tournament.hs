module ORS.Tournament where

import ORS.Types

addUserToTournament :: Tournament -> UserId -> Tournament
addUserToTournament t uId = t{players=(uId:(players t))}

delUserFromTournament :: Tournament -> UserId -> Tournament
delUserFromTournament t uId = t{players=[ x | x <- players t, x /= uId ]}

isReadyTournament :: Tournament -> Bool
isReadyTournament t = (seats t) == (length (players t))

calculateToBePlayedTournament :: Tournament -> Tournament
calculateToBePlayedTournament t =
	let (x:xs) = players t in t{toBePlayed = calc x xs}
		where	calc h l@(x:xs) = (map (\a -> (h,a)) l) ++ (calc x xs)
			calc _ [] = []
			
writeResultTournament :: Tournament -> UserId -> UserId -> Int -> Tournament
writeResultTournament t uId1 uId2 res = t{played=(((uId1,uId2),res):(played t))}
