module ORS.Game where

import ORS.Types
import ORS.Board
import ORS.SQL

import Data.Time.Clock
import System.Random(randomIO)

newGame :: User -> User -> String -> Time-> IO Game
newGame u1 u2 str t = do
	time <- getCurrentTime
	board <- newBoard str
	next <- randomIO
	let next_b = mod next 2
	let n_p = [(uid u1),(uid u2)]!!next_b
	let uid1 = uid u1
	let uid2 = uid u2
	if (n_p == uid1)
		then return $ Game 0 time uid1 uid2 n_p t board False False False str
		else return $ Game 0 time uid2 uid1 n_p t board False False False str
		
getOtherUserId :: Game -> UserId -> UserId
getOtherUserId g u
	| u == (p1 g) = (p2 g)
	| u == (p2 g) = (p1 g)
	| otherwise = 0
	
getParsableNextUser :: Game -> Int
getParsableNextUser g = let n_p = next g in let uid1 = p1 g in let uid2 = p2 g in
	if ( n_p == uid1 )
		then 1
		else 2

endGame :: Game -> UserId -> Int -> IO ()
endGame game winner reason = do
	updateGame reason winner (log_name game)


startGame :: Game -> Int -> Int -> IO ()
startGame g uuid1 uuid2 = do
	insertGame uuid1 uuid2 (log_name g) (truncate . timelimit $ g)

