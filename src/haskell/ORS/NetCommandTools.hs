{-# LANGUAGE DeriveDataTypeable #-}
module ORS.NetCommandTools(toNetCommand, fromNetCommand, NetCommandException) where

import ORS.Types
import Control.Exception
import Data.Typeable
import Data.Time


data NetCommandException = NetCommandException
	deriving (Show, Typeable)
	
instance Exception NetCommandException


fromNetCommand :: NetCommand -> String
fromNetCommand com = unwords $ translateCat com
	where
	translateCat :: NetCommand -> [String]
	translateCat (CONNECT c) = "CONNECT" : (translateConnect c)
	translateCat DISCONNECT = ["DISCONNECT"]
	translateCat (P_LIST p) = "P_LIST" : (translatePList p)
	translateCat (R_GAME r) = "R_GAME" : (translateRGame r)
	translateCat (IN_GAME gId i) = "IN_GAME" : (show gId) : (translateInGame i)
	translateCat (T_LIST t) = "T_LIST" : (translateTList t)
	translateCat (R_TOURNAMENT r) = "R_TOURNAMENT" : (translateRTournament r)

	translateConnect :: Connect -> [String]
	translateConnect C_ACK = ["ACK"]
	translateConnect C_ERROR = ["ERROR"]
	translateConnect _ = undefined

	translatePList :: P_list -> [String]
	translatePList Pl_BEGIN = ["BEGIN"]
	translatePList Pl_END = ["END"]
	translatePList (Pl_ENTRY gId usr) = [show gId, usr]
	translatePList _ = undefined

	translateRGame :: R_game -> [String]
	translateRGame Rg_sACK = ["ACK"]
	translateRGame Rg_sERROR = ["ERROR"]
	translateRGame Rg_sREJECT = ["REJECT"]
	translateRGame (FINALIZE gid t) = [show gid, (show . truncate) t]

	translateInGame :: In_game -> [String]
	translateInGame YOURTURN = ["YOURTURN"]
	translateInGame WAIT = ["WAIT"]
	translateInGame Ig_ACK = ["ACK"]
	translateInGame Ig_ERROR = ["ERROR"]
	translateInGame (PLAYED (x,y)) = ["PLAYED", show x, show y]
	translateInGame (Ig_END r) = ["END", show r]

	translateTList :: T_list -> [String]
	translateTList Tl_BEGIN = ["BEGIN"]
	translateTList Tl_END = ["END"]
	translateTList (Tl_ENTRY id seats free time) = map show [id, seats, free, truncate time]

	translateRTournament :: R_tournament -> [String]
	translateRTournament Rt_ACK = ["ACK"]
	translateRTournament Rt_ERROR = ["ERROR"]
	translateRTournament (Rt_END rank) = ["END", show rank]

toNetCommand :: String -> NetCommand
toNetCommand s = translateCat ( words s )
	where
	error_ =  throw NetCommandException

	translateCat :: [String] ->  NetCommand
	translateCat ("CONNECT":xs) = translateConnect xs
	translateCat ["DISCONNECT"] = DISCONNECT
	translateCat ("P_LIST":xs) = translatePList xs
	translateCat ("T_LIST":xs) = translateTList xs
	translateCat ("R_GAME":xs) = translateRGame xs
	translateCat ("IN_GAME":xs) = translateInGame xs
	translateCat ("R_TOURNAMENT":xs) = translateRTournament xs
	translateCat _ = error_

	translateConnect :: [String] ->  NetCommand
	translateConnect ("LOGWITH":u:p:[]) =  CONNECT $ LOGWITH u p
	translateConnect _ = error_

	translatePList :: [String] ->  NetCommand
	translatePList [] =  P_LIST $ Pl_REQ
	translatePList _ = error_

	translateTList :: [String] ->  NetCommand
	translateTList [] =  T_LIST $ Tl_REQ
	translateTList _ = error_

	translateRGame :: [String] ->  NetCommand
	translateRGame ("RANDOM":xs) = translateRGameRandom xs
	translateRGame ("WITH":xs) = translateRGameWith xs
	translateRGame (id:xs) =   R_GAME $ translateRGameAnswer (read id) xs
	translateRGame _ = error_

	translateRGameRandom :: [String] ->  NetCommand
	translateRGameRandom ("TIME":t:[]) = R_GAME $ RANDOM $ (fromIntegral ((read t)::Int))
	translateRGameRandom _ = error_

	translateRGameWith :: [String] ->  NetCommand
	translateRGameWith (id:"TIME":t:[]) = R_GAME $ WITH (read id) (fromIntegral ((read t)::Int))
	translateRGameWith _ = error_

	translateRGameAnswer :: Int -> [String] ->  R_game
	translateRGameAnswer id ["ACK"] =  Rg_cACK id
	translateRGameAnswer id ["ERROR"] =  Rg_cERROR id
	translateRGameAnswer id ["REJECT"] =  Rg_cREJECT id
	translateRGameAnswer _ _ = error_

	translateInGame :: [String] ->  NetCommand
	translateInGame (id:"PLAY":x:y:[]) = IN_GAME (read id) $ PLAY (read x,read y)
	translateInGame _ = error_

	translateRTournament :: [String] -> NetCommand
	translateRTournament ["CANCEL"] = R_TOURNAMENT $ Rt_CANCEL 
	translateRTournament (id:[]) = R_TOURNAMENT $ JOIN $ read id
	translateRTournament _ = error_
