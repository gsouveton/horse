{-|
	Module	: ORS.TCPListener
	Version	: 0.3
	State	: testing
	Author	: Jonathan GAYVALLET <jonathan.mael.gayvallet@gmail.com>
	Licence	: BSD
-}

module ORS.TCPListener(tcpListen) where

import ORS.ConcurrentTools
import ORS.Types
import ORS.NetCommandTools

import Prelude hiding (catch)
import Data.Time.Clock
import Network
import System.IO
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM
import Control.Monad
import Data.Maybe
import System.Timeout

-- | Opens a listening socket on a specified port and fork using the ClientHander on each client
tcpListen :: Integral a => ClientHandler -> Finalizer -> a -> IO ()
tcpListen clientHandler finalize port = withSocketsDo $ do
	putStrLn $ "Listening on TCP port " ++ show port
	bracket
		(listenOn $ PortNumber $ fromIntegral port) -- Open a TCP socket on port 'port'
		(sClose) -- Close the socket at the end (or in case of exception)
		(loop) -- Then run the loop
		
	where 	loop sock = accept sock >>= handle >> loop sock -- accept a connection from a client, handle it and repeat
		handle (h,n,p) = forkIO $ bracket
				(do
				putStrLn $ "Connexion from " ++ n ++ ":" ++ (show p)
				hSetEncoding h latin1
				hSetBuffering h $ LineBuffering -- Line/Block Buffering causes some issues
				inputTChan <- atomically $ newTChan
				myThreadId_ <- myThreadId
				clientHandler h inputTChan myThreadId_
				return inputTChan)
				
				(\chan -> do
				finalize chan
				hClose h -- close the Handle
				putStrLn $ "Connexion from " ++ n ++ ":" ++ (show p) ++ " closed")
				
				(inputLoop h)
				`catch` handleEOF
		inputLoop h chan = forever $ do
			time <- getCurrentTime
			line <- hGetLine h
			atomically $ writeTChan chan $ (line,time)
