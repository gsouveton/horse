module ORS.ClientManager where

import ORS.Types
import ORS.ConcurrentTools
import ORS.NetCommandTools

import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM
import System.IO
import Data.Time.Clock

newClientHandler :: TChan (Handle,TChan (String,UTCTime), ThreadId) -> ClientHandler
newClientHandler chan handle inputChan thread = atomically $ writeTChan chan $ (handle, inputChan, thread)

newFinalizer :: Finalizer
newFinalizer chan  = getCurrentTime >>= atomically . writeTChan chan . (,) (fromNetCommand DISCONNECT)
