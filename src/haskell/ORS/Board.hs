{-|
	Module	: ORS.Board
	Version	: 0.1
	State	: prototypes only
	Author	: Jonathan GAYVALLET <jonathan.mael.gayvallet@gmail.com>
	Licence	: BSD
	
-}

{-# LANGUAGE ForeignFunctionInterface #-}
module ORS.Board where
import Foreign
import Foreign.C.Types
import Foreign.C.String
import Foreign.Ptr

type Board = Ptr ()

-- | Create a new 'Board' (FFI alloc)
newBoard :: String -> IO Board
foreign import ccall "game.h G_Create"
	g_create :: CString -> IO Board
newBoard logname = do
	lognameC <- newCString logname
	g_create lognameC

-- | Delete a 'Board' (FFI free)
delBoard :: Board -> IO ()
foreign import ccall "game.h G_Destroy"
	g_destroy :: Board -> IO ()
delBoard b = g_destroy b

-- | Plays a move
playMove :: Board -> Int -> Int -> Int -> IO Int
foreign import ccall "game.h G_Play"
	g_play :: CInt -> CInt -> CInt -> Board -> IO CInt
playMove b x y p = g_play (fromIntegral (x-1)) (fromIntegral (y-1)) (fromIntegral p) b >>= return . fromIntegral

{-
	retour :
	1 | 2 -> numero du joueur
	0 -> partie bloquée/finie
	-1 -> mauvais coup
-}

-- | Give the final state of a game
finalState :: Board -> IO Int
foreign import ccall "game.h G_BoardState"
	g_boardstate :: Board -> IO CInt
finalState b = g_boardstate b >>= return . fromIntegral
{-
	retour :
	0 -> partie non finie
	1 -> p1 a gagné
	2 -> p2 '''''''
	3 -> draw

-}
