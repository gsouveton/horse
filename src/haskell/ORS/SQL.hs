{-# LANGUAGE FlexibleContexts #-}
module ORS.SQL where

import ORS.Types

import Database.HDBC.MySQL
import Database.HDBC
import Data.Convertible.Base
import Data.Hash.MD5
import Control.Monad
import Control.Exception


connect = connectMySQL defaultMySQLConnectInfo {
					mysqlHost	= "127.0.0.1",
					mysqlUser	= "projettuto",
					mysqlPassword	= "hellokitty",
					mysqlDatabase	= "projettuto",
					mysqlPort	= 3306
					}
					
login :: String -> String -> IO UserId
login username password = bracket
	(connect)
	(disconnect)
	(\conn -> do
	row <- withRTSSignalsBlocked $ do
		quickQuery' conn "SELECT * FROM clients WHERE username = ? AND password = ? LIMIT 0,1" [toSql username, toSql ( (md5s . Str) password)]
	case row of 
		(((SqlInteger x):_):_) -> return $ fromIntegral x
		_ -> return 0
	)

insertGame :: Int -> Int -> String -> Int -> IO ()
insertGame u1 u2 logname time = bracket 
	(connect)
	(disconnect)
	(\conn -> do
	withRTSSignalsBlocked $ do
		run conn "INSERT INTO games ( id , whitePlayer , blackPlayer , ended , winner , logname , timestamp , time ) VALUES ( NULL , ? , ? , 0 , 0 , ? , UNIX_TIMESTAMP() , ? )" [ toSql u1 , toSql u2 , toSql logname , toSql time ] 
	return ()
	)

updateGame :: Int -> Int -> String -> IO ()
updateGame ended winner logname = bracket 
	(connect) 
	(disconnect) 
	(\conn -> do 
	withRTSSignalsBlocked $ do
		run conn "UPDATE games SET ended = ? , winner = ? WHERE logname = ? " [ toSql ended , toSql winner, toSql logname ] 
	return ()
	)


mySQLDoQuery :: Convertible a SqlValue => String -> [a] -> ([[SqlValue]] -> b)-> IO b
mySQLDoQuery query args fun = bracket
	(connect)
	(disconnect)
	(\conn -> do
	row <- withRTSSignalsBlocked $ quickQuery' conn query ( map toSql args )
	return $ fun row)
