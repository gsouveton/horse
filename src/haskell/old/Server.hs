module Server (server) where
import Network
import System.IO
import System.IO.Error (isEOFError)
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Concurrent.MVar
import Control.Monad.STM
import Prelude hiding (catch)
import Control.Monad



import ConcurrentTools
import Command
import String2Command
import User

server mOutput = do
	putStrLn "Server thread, listening on port 8000"
	withSocketsDo $ do
	bracket (listenOn $ PortNumber 8000) (sClose) (loop)
	where	loop sock = accept sock >>= handle >> loop sock
		handle (h, n, p) = forkIO $ do
				putStrLn ("New connection from " ++ n ++ ":" ++ show p)
				manage mOutput h
				putStrLn ("Connection closed " ++ n ++ ":" ++ show p) >>  hClose h


manage mOutput h = do
		set@(cInput, cOutput, mInput, qi, qo, qiMVar, qoMVar) <- initClient h
		catch (loop cInput cOutput mInput mOutput) (handler)
		closeClient set
	where 	loop cInput cOutput mInput mOutput = do
			msg <- atomically $ readTChan cInput
			catch	( do
					cmd <- return $ string2command msg
					case cmd of
						CONNECT _ -> atomically $ writeTChan mOutput $ Left (cmd, mInput)
						_ -> atomically $ writeTChan mOutput $ Right cmd
				)
				(\e -> atomically $ writeTChan cOutput $ "Error on input : " ++ msg ++ " : " ++ show (e::SomeException))
			loop cInput cOutput mInput mOutput
		handler e
			| isEOFError e = return ()
			| otherwise = return ()
	
	
queueInput :: Handle -> TChan String -> IO ()
queueInput h input = do
	msg <- hGetLine h
	atomically $ writeTChan input msg
	queueInput h input

queueOutput :: Handle -> TChan String -> IO ()
queueOutput h output = do
	msg <- atomically $ readTChan output
	hPutStrLn h msg
	queueOutput h output

initClient :: Handle -> IO (TChan String, TChan String, TChan (Either (Command) (Command, User)), ThreadId, ThreadId, MVar Bool, MVar Bool)
initClient h = do
	qiMVar <- newEmptyMVar
	qoMVar <- newEmptyMVar
	hSetBuffering h LineBuffering
	hSetEncoding h latin1
	cInput <- atomically $ newTChan -- this channel proceeds what is received from the client
	cOutput <- atomically $ newTChan -- this channel proceeds what is to be sent to the client
	mInput <- atomically $ newTChan -- this channel proceeds what is reveived from the manager
	qi <- spawnMVar qiMVar $ queueInput h cInput -- handle -> input channel
	qo <- spawnMVar qoMVar $ queueOutput h cOutput -- output channel -> handle
	return (cInput, cOutput, mInput, qi, qo, qiMVar, qoMVar)

closeClient :: (TChan String, TChan String, TChan Command, ThreadId, ThreadId, MVar Bool, MVar Bool) -> IO ()
closeClient (cInput, cOutput, mInput, qi, qo, qiMVar, qoMVar) = do
	qiMVar' <- tryTakeMVar qiMVar
	qoMVar' <- tryTakeMVar qoMVar
	if (qiMVar' == Nothing) then killThread qi else return ()
	if (qoMVar' == Nothing) then killThread qo else return ()
