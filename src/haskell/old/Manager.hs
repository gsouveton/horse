module Manager(manager) where
--import qualified SQL -- Not on windows

import System.IO
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM
import Data.IORef

import Game
import Tournament
import User
import Command
import ConcurrentTools

manager clientToManager managerToGame managerToConsole gameToManager consoleToManager = do
	putStrLn "Manager Running"
	loop clientToManager managerToGame managerToConsole gameToManager consoleToManager [] [] [] []
	where loop clientToManager managerToGame managerToConsole gameToManager consoleToManager users available games tournaments = 
		do
		input <- atomically $ select clientToManager gameToManager
		case input of
			-- from nInput
			Left input -> putStrLn "input from the network threads"
			-- from gInput
			Right input -> putStrLn "input from the game manager"
		loop clientToManager managerToGame managerToConsole gameToManager consoleToManager users available games tournaments
