module FromCommand (fromCommand) where
import Command

fromCommand :: Command -> String
fromCommand c = unwords $ translateCat c

translateCat :: Command -> [String]
translateCat (CONNECT c) = "CONNECT" : (translateConnect c)
translateCat DISCONNECT = ["DISCONNECT"]
translateCat (P_LIST p) = "P_LIST" : (translatePList p)
translateCat (R_GAME r) = "R_GAME" : (translateRGame r)
translateCat (IN_GAME gid i) = "IN_GAME" : (show gid) : (translateInGame i)
translateCat (T_LIST t) = "T_LIST" : (translateTList t)
translateCat (R_TOURNAMENT r) = "R_TOURNAMENT" : (translateRTournament r)

translateConnect :: Connect -> [String]
translateConnect C_ACK = ["ACK"]
translateConnect C_ERROR = ["ERROR"]

translatePList :: P_list -> [String]
translatePList Pl_BEGIN = ["BEGIN"]
translatePList Pl_END = ["END"]
translatePList (Pl_ENTRY id usr) = [show id, usr]

translateRGame :: R_game -> [String]
translateRGame Rg_sACK = ["ACK"]
translateRGame Rg_sERROR = ["ERROR"]
translateRGame Rg_sREJECT = ["REJECT"]
translateRGame (FINALIZE gid t) = [show gid, show t]

translateInGame :: In_game -> [String]
translateInGame YOURTURN = ["YOURTURN"]
translateInGame WAIT = ["WAIT"]
translateInGame Ig_ACK = ["ACK"]
translateInGame Ig_ERROR = ["ERROR"]
translateInGame (PLAYED x y) = ["PLAYED", show x, show y]
translateInGame (Ig_END r) = ["END", show r]

translateTList :: T_list -> [String]
translateTList Tl_BEGIN = ["BEGIN"]
translateTList Tl_END = ["END"]
translateTList (Tl_ENTRY id seats free time) = map show [id, seats, free, time]

translateRTournament :: R_tournament -> [String]
translateRTournament Rt_ACK = ["ACK"]
translateRTournament Rt_ERROR = ["ERROR"]
translateRTournament (Rt_END rank) = ["END", show rank]
