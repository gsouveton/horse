module TCPTest where

import ConcurrentTools
import Network
import System.IO
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM
import Control.Monad

import System.Console.Readline


tcpListen clientHandler = do
	withSocketsDo $ bracket
		(listenOn $ PortNumber 8000)
		(sClose)
		(loop)

	where 	loop sock = accept sock >>= handle >> loop sock
		handle (h,n,p) = forkIO $
			do
				putStrLn $ "Connexion from " ++ n ++ (show p)
				hSetBuffering h NoBuffering
				clientHandler h
			`finally`
			do
				hClose h
				putStrLn $ "Connexion from " ++ n ++ (show p) ++ " closed"

simpleClientHandler h = do
	inputChan <- atomically $ newTChan
	inputThread <- spawn $ (inputLoop h inputChan)
	(clientLoop h inputChan) `finally` (killThread inputThread)

	where 	inputLoop h chan = do
			line <- hGetLine h
			atomically $ writeTChan chan line
			inputLoop h chan
		clientLoop h chan = do
