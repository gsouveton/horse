module TCPListener (tcpListen) where

import Network
import System.IO
import Control.Concurrent
import Control.Exception


tcpListen :: Int -> (Handle -> IO ()) -> IO ()
tcpListen port act = withSocketsDo $ do
	putStrLn $ "Server thread, listening on port " ++ show port
	withSocketsDo $ do
	bracket (listenOn $ PortNumber $ fromIntegral port) (sClose) (loop)
	where	loop sock = accept sock >>= handle >> loop sock
		handle (h, n, p) = forkIO $ do
				putStrLn ("New connection from " ++ n ++ ":" ++ show p)
				act h
				hClose h
				putStrLn ("Connection closed " ++ n ++ ":" ++ show p)
