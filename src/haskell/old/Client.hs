module Client (client) where

import System.IO
import System.IO.Error (isEOFError)
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Concurrent.MVar
import Control.Monad.STM
import Data.Maybe
import Prelude hiding (catch)
import Control.Monad

import Command
import ToCommand
import FromCommand

import ConcurrentTools


client :: TChan ClientToManager -> Handle -> IO ()
client toManager h = do
		set@(input, fromManager, qi, qiMVar) <- initClient h
		catch (mainLoop input toManager fromManager 0) (handler)
		closeClient set
	where handler e
		| isEOFError e = return ()
		| otherwise = putStrLn $ show e

mainLoop input toManager fromManager usrid = do
	msg <- atomically $ select input fromManager
	case msg of
		Left s -> putStrLn "Message from the client" --stringClient input output toManager fromManager usrid s
		Right c -> putStrLn "Message from the manager" --commandClient input output toManager fromManager usrid c
	--mainLoop input toManager fromManager usrid



queueInput :: Handle -> TChan String -> IO ()
queueInput h input = do
		msg <- hGetLine h
		atomically $ writeTChan input msg
		queueInput h input

initClient :: Handle -> IO (TChan String, TChan ManagerToClient, ThreadId, MVar Bool)
initClient h = do
	qiMVar <- newEmptyMVar
	hSetBuffering h NoBuffering
	hSetEncoding h latin1
	input <- atomically $ newTChan -- this channel proceeds what is received from the client
	fromManager <- atomically $ newTChan -- this channel proceeds what is reveived from the manager
	qi <- spawnMVar qiMVar $ do queueInput h input `finally` (hClose h) -- handle -> input channel
	return (input, fromManager, qi, qiMVar)

closeClient :: (TChan String, TChan ManagerToClient, ThreadId, MVar Bool) -> IO ()
closeClient (input, fromManager, qi, qiMVar) = do
	qiMVar <- tryTakeMVar qiMVar
	unless (isJust qiMVar) $ killThread qi
	return ()
