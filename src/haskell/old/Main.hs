module Main where

import System.IO
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM
import Control.Monad

import System.Console.Readline


import TCPListener
import Client
import Manager
import ConcurrentTools

main = do
	putStrLn "Othelo Rigged Server, initializing"
	putStrLn "Creating TChan for interprocess communication"
	clientToManager <- atomically $ newTChan -- to the manager
	managerToGame <- atomically $ newTChan -- to the game module
	managerToConsole <- atomically $ newTChan -- to the console
	gameToManager <- atomically $ newTChan -- from the game module
	consoleToManager <- atomically $ newTChan -- from the console
	putStrLn "Spawning threads"
	tcpserver <- spawn $ tcpListen 8000 $ client clientToManager
	manager <- spawn $ manager clientToManager managerToGame managerToConsole gameToManager consoleToManager
	--games <- spawn $ games gInput gOuput
	loop consoleToManager managerToConsole
	where loop consoleToManager managerToConsole = sequence_ $ repeat $ yield
	--	maybeLine <- readline "ORS # "
	--	case maybeLine of
	--		Nothing -> return ()
	--		Just "exit" -> return ()
	--		Just line -> do addHistory line
	--				loop consoleToManager managerToConsole
