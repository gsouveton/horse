module ConcurrentTools where
import Prelude hiding (catch)
import Network
import System.IO
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM


spawn :: IO () -> IO ThreadId
spawn f = do
	mainTID <- myThreadId
	forkIO $ catch f (\err -> throwTo mainTID (err::SomeException))

spawnMVar :: MVar Bool -> IO () -> IO ThreadId
spawnMVar m f = do
	mainTID <- myThreadId
	forkIO $ catch f (\err -> throwTo mainTID (err::SomeException)) `finally` (putMVar m True)
	

select :: TChan a -> TChan b -> STM (Either a b)
select ch1 ch2 = do
	a <- readTChan ch1; return (Left a)
	`orElse` do
	b <- readTChan ch2; return (Right b)

--select3 :: TChan a -> TChan b -> TChan c -> STM (Either (Either a b) c)
--select3 ch1 ch2 ch3 = do
--	a <- readTChan ch1; return (Left $ Left a)
--	`orElse` do
--		b <- readTChan ch2; return (Left $ Right b)
--		`orElse` do
--		c <- readTChan ch3; return (Right c)

