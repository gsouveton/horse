module User where
import Command

import System.IO
import Control.Exception
import Control.Concurrent
import Control.Concurrent.STM.TChan
import Control.Monad.STM

data User = User {
	id	:: Int,
	name	:: String,
	channel	:: TChan Command }

instance Show User where
	show ( User id name _ ) = "User " ++ show id ++ name
