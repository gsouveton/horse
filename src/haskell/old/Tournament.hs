module Tournament where

import Command

data Tournament = Tournament {
	id 	:: Int,
	seats	:: Int }
