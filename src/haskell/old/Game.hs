module Game where
import User
import Command


data GameState = GS_Request | GS_Accept | GS_Reject | GS_Error | GS_Running | GS_Finished
	deriving (Eq, Show)

data Game = Game{
	id		:: Int,
	u1		:: User,
	u2		:: User,
	state		:: GameState,
	time		:: Int}
	deriving (Show)
