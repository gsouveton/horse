module SQL where

import Database.HDBC.MySQL
import Database.HDBC

import Control.Monad

connect = connectMySQL defaultMySQLConnectInfo {
					mysqlHost	= "127.0.0.1",
					mysqlUser	= "projettuto",
					mysqlPassword	= "hellokitty",
					mysqlDatabase	= "projettuto",
					mysqlPort	= 3307
					}

login :: String -> String -> IO Bool
login username password = do
	conn <- connect
	rows <- withRTSSignalsBlocked $ do
		quickQuery' conn "SELECT * FROM clients WHERE username = ? AND password = ?" [toSql username, toSql password]
	hrows <-  mapM (\_ -> return [True]) rows
	disconnect conn
	return (hrows /= [])
