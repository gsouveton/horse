module ToCommand (toCommand) where
import Command

toCommand :: String -> Command
toCommand s = translateCat $ words s

error_ :: a
error_ = error "unable to translate"

translateCat :: [String] ->  Command
translateCat ("CONNECT":xs) = translateConnect xs
translateCat ["DISCONNECT"] = DISCONNECT
translateCat ("P_LIST":xs) = translatePList xs
translateCat ("T_LIST":xs) = translateTList xs
translateCat ("R_GAME":xs) = translateRGame xs
translateCat ("IN_GAME":xs) = translateInGame xs
translateCat ("R_TOURNAMENT":xs) = translateRTournament xs
translateCat _ = error_

translateConnect :: [String] ->  Command
translateConnect ("LOGWITH":u:p:[]) =  CONNECT $ LOGWITH u p
translateConnect _ = error_

translatePList :: [String] ->  Command
translatePList [] =  P_LIST $ Pl_REQ
translatePList _ = error_

translateTList :: [String] ->  Command
translateTList [] =  T_LIST $ Tl_REQ
translateTList _ = error_

translateRGame :: [String] ->  Command
translateRGame ("RANDOM":xs) = translateRGameRandom xs
translateRGame ("WITH":xs) = translateRGameWith xs
translateRGame (id:xs) =   R_GAME $ translateRGameAnswer (read id) xs
translateRGame _ = error_

translateRGameRandom :: [String] ->  Command
translateRGameRandom ("TIME":t:[]) = R_GAME $ RANDOM $ read t
translateRGameRandom _ = error_

translateRGameWith :: [String] ->  Command
translateRGameWith (id:"TIME":t:[]) = R_GAME $ WITH (read id) (read t)
translateRGameWith _ = error_

translateRGameAnswer :: Int -> [String] ->  R_game
translateRGameAnswer id ["ACK"] =  Rg_cACK id
translateRGameAnswer id ["ERROR"] =  Rg_cERROR id
translateRGameAnswer id ["REJECT"] =  Rg_cREJECT id
translateRGameAnswer _ _ = error_

translateInGame :: [String] ->  Command
translateInGame (id:"PLAY":x:y:[]) = IN_GAME (read id) $ PLAY (read x) (read y)
translateInGame _ = error_

translateRTournament :: [String] -> Command
translateRTournament ["CANCEL"] = R_TOURNAMENT $ Rt_CANCEL 
translateRTournament ("JOIN":id:[]) = R_TOURNAMENT $ JOIN $ read id
translateRTournament _ = error_
