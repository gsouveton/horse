module Command where

--This Module describe all valid commands the server can send / receive--

data Command = 	  CONNECT Connect
		| DISCONNECT
		| P_LIST P_list 
		| R_GAME R_game
		| IN_GAME Int In_game
		| T_LIST T_list
		| R_TOURNAMENT R_tournament
	deriving (Eq, Show)

data Connect =	  LOGWITH String String
		| C_ACK
		| C_ERROR
	deriving (Eq, Show)

data P_list = Pl_REQ | Pl_BEGIN | Pl_END | Pl_ENTRY Int String
	deriving (Eq, Show)

data R_game = Rg_sACK
		| Rg_sERROR
		| Rg_sREJECT
		| Rg_cACK Int
		| Rg_cERROR Int
		| Rg_cREJECT Int
		| RANDOM Int
		| WITH Int Int
		| FINALIZE Int Int
	deriving (Eq, Show)

data In_game =	  Ig_ACK
		| Ig_ERROR
		| YOURTURN 
		| WAIT
		| PLAY Int Int
		| PLAYED Int Int
		| Ig_END Reason
	deriving (Eq, Show)

data Reason =	  WIN
		| LOSE
		| DRAW
		| ERROR
		| TIMEOUT
	deriving (Eq,Show)

data T_list = Tl_REQ | Tl_BEGIN | Tl_END | Tl_ENTRY Int Int Int Int
	deriving (Eq, Show)

data R_tournament = JOIN Int | Rt_ACK | Rt_ERROR | Rt_CANCEL | Rt_END Int
	deriving (Eq, Show)
