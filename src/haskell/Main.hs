module Main where

import ORS.ClientManager
import ORS.TCPListener
import ORS.Types
import ORS.Manager
import ORS.Env
import ORS.ConcurrentTools
import ORS.Console


import System
import System.IO
import Control.Concurrent
import Control.Monad.STM
import Control.Monad
import Control.Concurrent.STM.TChan
import Control.Concurrent.STM.TVar

main = do
	[port] <- getArgs
	registerTChan <- atomically $ newTChan
	env <- atomically $ newTVar initEnv
	let finalizer = newFinalizer
	let clientHandler = newClientHandler registerTChan
	spawn $ tcpListen clientHandler finalizer (read port)
	spawn $ manager registerTChan env
	--spawn $ console env
	lock

iMain = do
	putStrLn "port : "
	port <- getLine
	registerTChan <- atomically $ newTChan
	env <- atomically $ newTVar initEnv
	let finalizer = newFinalizer
	let clientHandler = newClientHandler registerTChan
	spawn $ tcpListen clientHandler finalizer (read port)
	spawn $ manager registerTChan env
	return env
