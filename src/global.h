#ifndef _GLOBAL_H_
#define _GLOBAL_H_

// Standart C libraries.

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

// Types definitions

#include "types.h"

// Functions definitions.

#include "memory.h"
#include "files.h"
#include "print.h"
#include "routines.h"

#endif
