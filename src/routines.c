#include "global.h"
#include "routines.h"

double R_Floor ( double x )
{
	return ( int ) ( x + 0x40000000 ) - 0x40000000;
}

void * R_Memset( void * dest , int c , size_t count )
{
	while ( count -- )
		( ( char * ) dest ) [ count ] = c;
	return dest;
}

void * R_Memcpy ( void * dest , const void * src , size_t count )
{
	while ( count -- )
		( ( char * ) dest ) [ count ] = ( ( char * ) src ) [ count ];
	return dest;
}
