#ifndef _MEMORY_H_
#define _MEMORY_H_

#include "global.h"
#include "routines.h"
#include "print.h"

void M_BadAlloc ( char * l );

void * M_Alloc ( size_t s , char * l );

#endif
