#ifndef _GAME_H_
#define _GAME_H_

#include "global.h"
#include "types.h"

// Public functions //

typedef game_t* Board;

game_t * G_Create ( char * );

void G_Destroy ( game_t * );

int G_Play ( int , int , player_t , game_t * );

int G_BoardState ( game_t * );


// Private part //

int G_Clamped ( int );

move_t * VMOV ( int , int , player_t );

#ifndef DMOV
	#define DMOV(x) free(x)
#endif

void G_InitialiseGrid ( grid_t grid );

int G_OtherPlayer ( player_t );

int G_CountPoints ( grid_t , player_t );

void G_GridConvert ( grid_t , char [ 8 ] , player_t );

void G_GRotate ( char c [ 8 ] );

void G_ApplyMove ( move_t * , game_t * );

int G_IsValidMove( move_t * , game_t * );

#ifndef gamelog_t
	typedef FILE * gamelog_t;
#endif

int G_ExportGrid ( char * , grid_t );

int G_NextPlayer ( game_t * );

#endif
