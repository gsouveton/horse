#ifndef _TYPES_H_
#define _TYPES_H_

typedef enum
{
        PL_NONE = 0,
        PL_WHITE = 1,
        PL_BLACK = 2
} player_t;

typedef player_t grid_t [ 64 ];

typedef struct
{
        unsigned short x;
        unsigned short y;
        player_t player;
} move_t;

typedef struct
{
        char               logname [ 81 ]; // This size matches with database's one.

        grid_t             grid;

        player_t           activePlayer;

        int                ended;   // Game's finished
        player_t           winner;  //
} game_t;

#endif
