#include "global.h"

#include "print.h"

#ifndef min
	#define min(x,y) (x<y?x:y)
#endif

#ifndef max
	#define max(x,y) (x>y?x:y)
#endif

double R_Floor ( double x );

void * R_Memset( void * dest , int c , size_t count );

void * R_Memcpy( void *dest , const void * src , size_t count );

void R_Error ( int , char * , char * );
