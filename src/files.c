#include "files.h"

void F_CannotOpen ( const char * name )
{
	P_PrintLog ( "Fatal error : cannot allocate open file '%s'\n" , name );
}

FILE * F_Open ( const char * name, const char * type )
{
	FILE * file = fopen ( name , type );
	if ( file == NULL )
		F_CannotOpen ( name );
	return file;
}

void F_Close ( FILE * f )
{
	fclose ( f );
}
