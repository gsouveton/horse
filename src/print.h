#include "global.h"
#include "files.h"

#ifndef LOGFILE
	#define LOGFILE "server.log"
#endif

void P_PrintLog ( const char * format , ... );
void P_PrintDebug ( const char * format , ... );

#ifndef P_PrintError
	#define P_PrintError P_PrintDebug
#endif

void P_Error ( const char * format , ... );

void P_Print ( const char * format , ... );
void P_PrintFile ( const char * filename , const char * format , ... );
